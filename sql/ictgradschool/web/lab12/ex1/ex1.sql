-- Answers to exercise 1 questions
-- A: depts offering courses:
SELECT DISTINCT dept FROM unidb_courses;

-- B: semesters being attended:
SELECT DISTINCT semester FROM unidb_attend;

-- C: courses being attended
SELECT DISTINCT concat(dept, ' ', num) AS courses_attended FROM unidb_attend;

-- D: student names and country, ordered by first name
SELECT concat(fname, ' ', lname) AS name, country FROM unidb_students ORDER BY fname;

-- E. List the student names and mentors, ordered by mentors
SELECT concat(s.fname, ' ', s.lname) AS name, concat(m.fname, ' ', m.lname) AS mentor FROM unidb_students as s, unidb_students AS m
WHERE s.mentor = m.id ORDER BY s.mentor;

-- F. List the lecturers, ordered by office
SELECT concat(fname, ' ', lname) AS name FROM unidb_lecturers ORDER BY office;

-- G. List the staff whose staff number is greater than 500
SELECT concat(fname, ' ', lname) AS name, staff_no FROM unidb_lecturers WHERE staff_no>500;

-- H. List the students whose id is greater than 1668 and less than 1824
-- sounds like should be non-inclusive
SELECT concat(fname, ' ', lname) AS name, id FROM unidb_students WHERE id BETWEEN 1669 AND 1823;

-- I. List the students from NZ, Australia and US
SELECT concat(fname, ' ', lname) AS name, country FROM unidb_students WHERE country IN ('NZ', 'AU', 'US');

-- J. List the lecturers in G Block
SELECT concat(fname, ' ', lname) AS name, office FROM unidb_lecturers WHERE (office Like 'G%');

-- K. List the courses not from the Computer Science Department
SELECT concat(dept, num) AS course FROM unidb_courses WHERE dept != 'comp';

-- L. List the students from France or Mexico
SELECT concat(fname, ' ', lname) AS name FROM unidb_students WHERE country IN ('FR', 'MX');