-- Answers to exercise 2 questions

-- A. What are the names of the students who attend COMP219?
SELECT concat(s.fname, ' ', s.lname) AS Name
    FROM unidb_students AS s, unidb_attend AS a
    WHERE s.id = a.id AND a.dept = 'comp' AND a.num = '219';

-- B. What are the names of the student reps that are not from NZ?
SELECT concat(s.fname, ' ', s.lname) as Rep, s.country
FROM unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id AND s.country != 'NZ';

-- C. Where are the offices for the lecturers of 219?
SELECT concat(l.fname, ' ', l.lname) as Lecturer, l.office
FROM unidb_lecturers as l,
     unidb_teach as t
WHERE t.dept = 'comp'
  AND t.num = '219'
  AND l.staff_no = t.staff_no;

-- D. What are the names of the students taught by Te Taka?
SELECT DISTINCT
     concat(s.fname, ' ', s.lname) AS Student
FROM unidb_students as s,
     unidb_attend as a,
     unidb_lecturers as l,
     unidb_teach as t
WHERE s.id = a.id AND
      l.staff_no = t.staff_no AND
      t.dept = a.dept ANd
      t.num = a.num AND
      l.fname = 'Te Taka';

-- E. List the students and their mentors
SELECT concat(s.fname, ' ', s.lname) AS Student, concat(m.fname, ' ', m.lname) AS Mentor
FROM unidb_students AS s, unidb_students AS m
WHERE s.mentor = m.id
ORDER BY m.fname;

-- F. Name the lecturers whose office is in G-Block as well naming the students that are not from NZ
-- this seems like kinda a pointless thing to do but...
SELECT  concat(fname, ' ', lname) as Name
FROM unidb_lecturers
WHERE office LIKE 'G%'
UNION
SELECT concat(fname, ' ', lname) AS Name
FROM unidb_students
WHERE country != 'NZ';

-- G. List the course coordinator and student rep for COMP219
SELECT concat(l.fname, ' ', l.lname) as Coord, concat(s.fname, ' ', s.lname) AS Rep
FROM unidb_students AS s, unidb_lecturers AS l, unidb_courses AS c
WHERE c.dept = 'comp' AND c.num = '219' AND l.staff_no = c.coord_no AND s.id = c.rep_id;