  ALTER TABLE sports_teams
 DROP FOREIGN KEY sports_teams_ibfk_2,
  DROP FOREIGN KEY sports_teams_ibfk_1;
  ALTER TABLE sports_players
  DROP FOREIGN KEY sports_players_ibfk_1,
  DROP FOREIGN KEY sports_players_ibfk_2;

DROP TABLE IF EXISTS sports_league;
DROP TABLE IF EXISTS sports_teams;
DROP TABLE IF EXISTS sports_players;

CREATE TABLE sports_league (
  league_name varchar(20) NOT NULL,
  PRIMARY KEY (league_name)
);

CREATE TABLE sports_teams (
  id int NOT NULL ,
  name varchar(20) NOT NULL ,
  city varchar(20),
  points int,
  league varchar(20) NOT NULL ,
  captain int ,
  PRIMARY KEY (id),
  UNIQUE (name),
   FOREIGN KEY (league) REFERENCES sports_league(league_name)
);

CREATE TABLE sports_players (
  id int NOT NULL,
  fname varchar(20),
  lname varchar(20),
  age int,
  nationality char(2),
  team int,
  league varchar(20),
  PRIMARY KEY (id),
   FOREIGN KEY (team) REFERENCES sports_teams(id),
   FOREIGN KEY (league) REFERENCES sports_league(league_name)
);



INSERT INTO sports_league VALUES ('NHL');

INSERT INTO sports_teams VALUES ( '1', 'Penguins', 'Pittsburgh', '87', 'NHL', '87'),
                                ( '2', 'Capitals', 'Washington', 77, 'NHL', 8),
                                ( '67', 'Maple Leafs', 'Toronto', 81, 'NHL', NULL);

  INSERT INTO sports_players VALUES  ('87', 'Sidney', 'Crosby', '31', 'CA', 1, 'NHL'),
                                     ('71', 'Evgeni', 'Malkin', 32, 'RU', 1, 'NHL'),
                                     (8, 'Alex', 'Ovechkin', 32, 'RU', 2, 'NHL'),
                                     (19, 'Nicklas', 'Backstrom', 31, 'SE', 2, 'NHL'),
                                     (34, 'Auston', 'Matthews', 21, 'US', 67, 'NHL'),
                                     (16, 'Mitch', 'Marner', 21, 'CA', 67, 'NHL'),
                                     (119, 'William', 'Nylander', 22, 'SE', 67, 'NHL');

  ALTER TABLE sports_teams add FOREIGN KEY (captain) REFERENCES sports_players(id);